# Copyright (C) 2021  European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS

# Most modules only need to be built for x86_64
ARCH_FILTER += linux-x86_64

REQUIRED += sequencer
ifneq ($(strip $(SEQUENCER_DEP_VERSION)),)
  sequencer_VERSION=$(SEQUENCER_DEP_VERSION)
endif

APP := dtlhighlevelApp
APPDB := $(APP)/Db
APPSRC := $(APP)/src

SOURCES += $(APPSRC)/orchestration_tank010.st

TEMPLATES += $(wildcard $(APPDB)/*.db)

SCRIPTS += $(wildcard ../iocsh/*.iocsh)
SCRIPTS += $(wildcard ../iocsh/*.cmd)

.PHONY: db
db:

.PHONY: vlibs
vlibs:
